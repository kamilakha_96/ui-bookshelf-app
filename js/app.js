let listSelesaiDibaca = document.getElementById("list-sudah-dibaca");
let listBelumDibaca = document.getElementById("list-belum-dibaca");
let bukuBelumDibaca = document.getElementsByName("buku-belum-dibaca");
let bukuSelesaiDibaca = document.getElementsByName("buku-selesai-dibaca");
let parents = null;
let selectedData = null;
let items = localStorage.getItem("items") ? JSON.parse(localStorage.getItem("items")) : [];

getLocalStorage();

function getLocalStorage() {
    if (items) {
        tambahBuku(items);
    }
}

function onFormSubmit() {
    let formData = {}
    formData["judul"] = document.getElementById("judul").value;
    formData["penulis"] = document.getElementById("penulis").value;
    formData["tahun"] = document.getElementById("tahun").value;
    formData["checked"] = document.getElementById("cek-buku").checked;
    formData["id"] = +new Date();
    if (formData.judul == "" && formData.penulis == "" && formData.tahun == "") {
        return false;
    }
    if (selectedData == null) {
        if (formData.judul == "") {
            return alert("Tolong isikan kolom judul");
        }
        if (formData.penulis == "") {
            return alert("Tolong isikan kolom penulis");
        }
        if (formData.tahun == "") {
            return alert("Tolong isikan kolom tahun");
        }
        items.push(formData);
        localStorage.setItem("items", JSON.stringify(items));
        tambahBuku(items);
    } else {
        updateBuku(formData);
    }
    resetForm();
}

function tambahBuku(items) {
    listSelesaiDibaca.innerHTML = "";
    listBelumDibaca.innerHTML = "";
    items.forEach(function (item) {
        if (item.checked === false) {
            let belumDibaca = document.createElement("li");
            belumDibaca.setAttribute("name", "buku-belum-dibaca");
            belumDibaca.setAttribute("id", item.id);
            belumDibaca.innerHTML =
                `<table>
                <tr class="incomplete">
                    <td>${item.judul}</td>
                </tr>
                <tr>
                    <td> ${item.penulis} </td>
                </tr>
                <tr>
                    <td> ${item.tahun} </td>
                </tr>
                <a class="material-icons-outlined" style="font-size: 20px; float: right;"
                    onclick="changeStatus(this)">
                    done
                </a>
                <a onclick="editBuku(this)" class="material-icons-outlined" style="font-size: 20px; float: right;">
                    edit
                </a>
                <button name="removeBtn" onclick="removeBuku(this)" class="material-icons-outlined" style="font-size: 20px; float: right;">
                    delete
                </button>
                </table>`
            listBelumDibaca.append(belumDibaca);
        } else {
            let selesaiDibaca = document.createElement("li");
            selesaiDibaca.setAttribute("name", "buku-selesai-dibaca");
            selesaiDibaca.setAttribute("id", item.id);
            selesaiDibaca.innerHTML =
                `<table>
                <tr class="complete">
                    <td>${item.judul}</td>
                </tr>
                <tr>
                    <td> ${item.penulis} </td>
                </tr>
                <tr>
                    <td> ${item.tahun} </td>
                </tr>
                <a class="material-icons-outlined" style="font-size: 20px; float: right;"
                    onclick="changeStatus(this)">
                    done
                </a>
                <a onclick="editBuku(this)" class="material-icons-outlined" style="font-size: 20px; float: right;">
                    edit
                </a>
                <button name="removeBtn" onclick="removeBuku(this)" class="material-icons-outlined" style="font-size: 20px; float: right;">
                    delete
                </button>
                </table>`
            listSelesaiDibaca.append(selesaiDibaca);
        };
    })
};

function resetForm() {
    document.getElementById("judul").value = "";
    document.getElementById("penulis").value = "";
    document.getElementById("tahun").value = "";
    document.getElementById("cek-buku").checked = false;
    selectedData = null;
}

function editBuku(td) {
    parents = td.parentElement;
    selectedData = td.parentElement.children[3].childNodes[1];
    document.getElementById("judul").value = selectedData.childNodes[0].innerText;
    document.getElementById("penulis").value = selectedData.childNodes[2].innerText;
    document.getElementById("tahun").value = selectedData.childNodes[4].innerText;
    document.getElementById("cek-buku").checked = selectedData.childNodes[0].className == "complete" ? true : false;
}

function removeBuku(td) {
    items = items.filter(function (item) {
        return item.id != td.parentElement.id;
    })
    localStorage.setItem("items", JSON.stringify(items));
    td.parentElement.remove();
}

function updateBuku(formData) {
    selectedData.childNodes[0].innerText = formData["judul"];
    selectedData.childNodes[2].innerText = formData["penulis"];
    selectedData.childNodes[4].innerText = formData["tahun"];
    let checking = formData["checked"] == true ? "complete" : "incomplete";
    if (checking != selectedData.childNodes[0].className) {
        selectedData.childNodes[0].className = checking;
        if (parents.parentElement.id == "list-belum-dibaca") {
            parents.attributes.name.nodeValue = "buku-selesai-dibaca";
            listSelesaiDibaca.append(parents);
        } else {
            parents.attributes.name.nodeValue = "buku-belum-dibaca";
            listBelumDibaca.append(parents);
        }
    }
    data = items.filter(function (item) {
        return item.id == parents.id;
    });
    data.forEach(function (i) {
        i.judul = formData["judul"];
        i.penulis = formData["penulis"];
        i.tahun = formData["tahun"];
        i.checked = formData["checked"];
        i.id = i.id
    })
    localStorage.setItem("items", JSON.stringify(items));
}

function changeStatus(td) {
    let checked = true;
    if (td.parentElement.parentElement.id == "list-belum-dibaca") {
        td.parentElement.children[3].childNodes[1].childNodes[0].className == "complete"
        checked = true;
        td.parentElement.attributes.name.nodeValue = "buku-selesai-dibaca";
        listSelesaiDibaca.append(td.parentElement);
    } else {
        td.parentElement.children[3].childNodes[1].childNodes[0].className == "incomplete"
        checked = false;
        td.parentElement.attributes.name.nodeValue = "buku-belum-dibaca";
        listBelumDibaca.append(td.parentElement);
    }
    data = items.filter(function (item) {
        return item.id == td.parentElement.id;
    });
    data.forEach(function (i) {
        i.checked = checked;
    })
    localStorage.setItem("items", JSON.stringify(items));
}

function searchList() {
    let filter = "";
    let li = "";
    let a = "";
    let i = "";
    let txtValue = "";
    let input = document.getElementById('cari-buku');
    filter = input.value.toUpperCase();
    li = document.querySelectorAll("li");

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("td")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}